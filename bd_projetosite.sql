-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 07-Fev-2021 às 20:45
-- Versão do servidor: 10.4.17-MariaDB
-- versão do PHP: 7.3.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `bd_projetosite`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `fat_pedido_cobranca`
--

CREATE TABLE `fat_pedido_cobranca` (
  `id` int(11) NOT NULL,
  `pedido_id` int(11) NOT NULL,
  `n_parcela` int(11) NOT NULL,
  `vencimento` date NOT NULL,
  `status` int(11) NOT NULL,
  `ativo` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `fat_saida`
--

CREATE TABLE `fat_saida` (
  `id` int(11) NOT NULL,
  `nota` int(11) NOT NULL,
  `serie` int(11) NOT NULL,
  `data` datetime NOT NULL,
  `empresa_id` int(11) NOT NULL,
  `pedido_id` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `produto_id` int(11) NOT NULL,
  `qtd` int(11) NOT NULL,
  `valor` float NOT NULL,
  `valor_total` float NOT NULL,
  `desconto` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `fat_status_pag`
--

CREATE TABLE `fat_status_pag` (
  `id` int(11) NOT NULL,
  `descricao` varchar(50) NOT NULL,
  `ativo` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `sys_aplicacao`
--

CREATE TABLE `sys_aplicacao` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `url` varchar(250) NOT NULL,
  `icone` varchar(250) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `ativo` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `sys_cliente`
--

CREATE TABLE `sys_cliente` (
  `id` int(11) NOT NULL,
  `dados_usuario_id` int(11) NOT NULL,
  `user` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `sys_empresa`
--

CREATE TABLE `sys_empresa` (
  `id` int(11) NOT NULL,
  `dados_usuario_id` int(11) NOT NULL,
  `user` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `sys_empresa_config`
--

CREATE TABLE `sys_empresa_config` (
  `id` int(11) NOT NULL,
  `empresa_id` int(11) NOT NULL,
  `logo` varchar(250) NOT NULL,
  `descricao` varchar(250) NOT NULL,
  `cor_primaria` varchar(10) NOT NULL,
  `cor_secundaria` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `sys_menu`
--

CREATE TABLE `sys_menu` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `empresa_id` int(11) NOT NULL,
  `ativo` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `sys_permissao`
--

CREATE TABLE `sys_permissao` (
  `usuario` varchar(100) NOT NULL,
  `aplicacao` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `sys_usuario`
--

CREATE TABLE `sys_usuario` (
  `user` varchar(100) NOT NULL,
  `pw` varchar(8) NOT NULL,
  `ativo` bit(1) NOT NULL,
  `tipo` int(11) NOT NULL,
  `tentativas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `sys_usuario_log`
--

CREATE TABLE `sys_usuario_log` (
  `user` varchar(100) NOT NULL,
  `data` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_carrinho`
--

CREATE TABLE `tbl_carrinho` (
  `id` int(11) NOT NULL,
  `empresa_id` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `dt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_carrinho_item`
--

CREATE TABLE `tbl_carrinho_item` (
  `id` int(11) NOT NULL,
  `carrinho_id` int(11) NOT NULL,
  `produto_id` int(11) NOT NULL,
  `qtd` int(11) NOT NULL,
  `valor` float NOT NULL,
  `valor_frete` float NOT NULL,
  `desconto` float NOT NULL,
  `tipo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_categoria`
--

CREATE TABLE `tbl_categoria` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `nivel` int(11) NOT NULL,
  `ativo` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_cupom`
--

CREATE TABLE `tbl_cupom` (
  `id` int(11) NOT NULL,
  `empresa_id` int(11) NOT NULL,
  `referencia` varchar(6) NOT NULL,
  `ini` datetime NOT NULL,
  `fim` datetime NOT NULL,
  `qtd` int(11) NOT NULL,
  `desconto` float NOT NULL,
  `ativo` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_cupom_pedido`
--

CREATE TABLE `tbl_cupom_pedido` (
  `pedido_id` int(11) NOT NULL,
  `cupom_id` int(11) NOT NULL,
  `dt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_dados_usuario`
--

CREATE TABLE `tbl_dados_usuario` (
  `id` int(11) NOT NULL,
  `nome` varchar(250) NOT NULL,
  `dt_nasc` date NOT NULL,
  `tipo_pessoa` char(1) NOT NULL,
  `documento` varchar(25) NOT NULL,
  `email` varchar(100) NOT NULL,
  `contato` varchar(20) NOT NULL,
  `cep` varchar(10) NOT NULL,
  `end` varchar(100) NOT NULL,
  `bairro` varchar(100) NOT NULL,
  `cidade` varchar(20) NOT NULL,
  `uf` varchar(2) NOT NULL,
  `pais` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_empresa_rede_social`
--

CREATE TABLE `tbl_empresa_rede_social` (
  `id` int(11) NOT NULL,
  `empresa_id` int(11) NOT NULL,
  `rede_social_id` int(11) NOT NULL,
  `url` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_entrada`
--

CREATE TABLE `tbl_entrada` (
  `id` int(11) NOT NULL,
  `empresa_id` int(11) NOT NULL,
  `produto_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `nota` int(11) NOT NULL,
  `serie` int(11) NOT NULL,
  `qtd` int(11) NOT NULL,
  `dt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_estoque`
--

CREATE TABLE `tbl_estoque` (
  `id` int(11) NOT NULL,
  `empresa_id` int(11) NOT NULL,
  `produto_id` int(11) NOT NULL,
  `qtd` int(11) NOT NULL,
  `dt_atualizacao` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_pedido`
--

CREATE TABLE `tbl_pedido` (
  `id` int(11) NOT NULL,
  `tipo_cobr_id` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `empresa_id` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `dt` datetime NOT NULL,
  `valor_total` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_pedido_item`
--

CREATE TABLE `tbl_pedido_item` (
  `id` int(11) NOT NULL,
  `pedido_id` int(11) NOT NULL,
  `produto_id` int(11) NOT NULL,
  `qtd` int(11) NOT NULL,
  `valor` float NOT NULL,
  `valor_frete` int(11) NOT NULL,
  `valor_total` float NOT NULL,
  `desconto` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_produto`
--

CREATE TABLE `tbl_produto` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `descricao` varchar(250) NOT NULL,
  `valor` float NOT NULL,
  `valor_base` float NOT NULL,
  `data_cadastro` datetime NOT NULL,
  `usuario_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_produto_categoria`
--

CREATE TABLE `tbl_produto_categoria` (
  `id` int(11) NOT NULL,
  `categoria_id` int(11) NOT NULL,
  `produto_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_produto_imagem`
--

CREATE TABLE `tbl_produto_imagem` (
  `id` int(11) NOT NULL,
  `produto_id` int(11) NOT NULL,
  `imagem` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_promocao`
--

CREATE TABLE `tbl_promocao` (
  `produto_id` int(11) NOT NULL,
  `descricao` varchar(250) NOT NULL,
  `novo_valor` float NOT NULL,
  `ini` datetime NOT NULL,
  `fim` datetime NOT NULL,
  `ativo` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_rede_social`
--

CREATE TABLE `tbl_rede_social` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `logo` varchar(250) NOT NULL,
  `url_base` varchar(250) NOT NULL,
  `ativo` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_status_pedido`
--

CREATE TABLE `tbl_status_pedido` (
  `id` int(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  `faturado` bit(1) NOT NULL,
  `cancelado` bit(1) NOT NULL,
  `pago` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_tipo_cobr`
--

CREATE TABLE `tbl_tipo_cobr` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `tipo` int(11) NOT NULL,
  `max_parcelas` int(11) NOT NULL,
  `ativo` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `fat_pedido_cobranca`
--
ALTER TABLE `fat_pedido_cobranca`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pedido_id` (`pedido_id`),
  ADD KEY `status` (`status`);

--
-- Índices para tabela `fat_saida`
--
ALTER TABLE `fat_saida`
  ADD PRIMARY KEY (`id`),
  ADD KEY `empresa_id` (`empresa_id`),
  ADD KEY `pedido_id` (`pedido_id`),
  ADD KEY `cliente_id` (`cliente_id`),
  ADD KEY `produto_id` (`produto_id`);

--
-- Índices para tabela `fat_status_pag`
--
ALTER TABLE `fat_status_pag`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `sys_aplicacao`
--
ALTER TABLE `sys_aplicacao`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_id` (`menu_id`);

--
-- Índices para tabela `sys_cliente`
--
ALTER TABLE `sys_cliente`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dados_usuario_id` (`dados_usuario_id`),
  ADD KEY `user` (`user`);

--
-- Índices para tabela `sys_empresa`
--
ALTER TABLE `sys_empresa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dados_usuario_id` (`dados_usuario_id`),
  ADD KEY `user` (`user`);

--
-- Índices para tabela `sys_empresa_config`
--
ALTER TABLE `sys_empresa_config`
  ADD PRIMARY KEY (`id`),
  ADD KEY `empresa_id` (`empresa_id`);

--
-- Índices para tabela `sys_menu`
--
ALTER TABLE `sys_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `empresa_id` (`empresa_id`);

--
-- Índices para tabela `sys_permissao`
--
ALTER TABLE `sys_permissao`
  ADD KEY `usuario` (`usuario`),
  ADD KEY `aplicacao` (`aplicacao`);

--
-- Índices para tabela `sys_usuario`
--
ALTER TABLE `sys_usuario`
  ADD PRIMARY KEY (`user`);

--
-- Índices para tabela `sys_usuario_log`
--
ALTER TABLE `sys_usuario_log`
  ADD KEY `foreign_key_user` (`user`);

--
-- Índices para tabela `tbl_carrinho`
--
ALTER TABLE `tbl_carrinho`
  ADD PRIMARY KEY (`id`),
  ADD KEY `empresa_id` (`empresa_id`),
  ADD KEY `cliente_id` (`cliente_id`);

--
-- Índices para tabela `tbl_carrinho_item`
--
ALTER TABLE `tbl_carrinho_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `produto_id` (`produto_id`),
  ADD KEY `carrinho_id` (`carrinho_id`);

--
-- Índices para tabela `tbl_categoria`
--
ALTER TABLE `tbl_categoria`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `tbl_cupom`
--
ALTER TABLE `tbl_cupom`
  ADD PRIMARY KEY (`id`),
  ADD KEY `empresa_id` (`empresa_id`);

--
-- Índices para tabela `tbl_cupom_pedido`
--
ALTER TABLE `tbl_cupom_pedido`
  ADD KEY `pedido_id` (`pedido_id`),
  ADD KEY `cupom_id` (`cupom_id`);

--
-- Índices para tabela `tbl_dados_usuario`
--
ALTER TABLE `tbl_dados_usuario`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `tbl_empresa_rede_social`
--
ALTER TABLE `tbl_empresa_rede_social`
  ADD PRIMARY KEY (`id`),
  ADD KEY `empresa_id` (`empresa_id`),
  ADD KEY `rede_social_id` (`rede_social_id`);

--
-- Índices para tabela `tbl_entrada`
--
ALTER TABLE `tbl_entrada`
  ADD PRIMARY KEY (`id`),
  ADD KEY `empresa_id` (`empresa_id`),
  ADD KEY `produto_id` (`produto_id`),
  ADD KEY `usuario_id` (`usuario_id`);

--
-- Índices para tabela `tbl_estoque`
--
ALTER TABLE `tbl_estoque`
  ADD PRIMARY KEY (`id`),
  ADD KEY `empresa_id` (`empresa_id`),
  ADD KEY `produto_id` (`produto_id`);

--
-- Índices para tabela `tbl_pedido`
--
ALTER TABLE `tbl_pedido`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tipo_cobr_id` (`tipo_cobr_id`),
  ADD KEY `empresa_id` (`empresa_id`),
  ADD KEY `cliente_id` (`cliente_id`),
  ADD KEY `status` (`status`);

--
-- Índices para tabela `tbl_pedido_item`
--
ALTER TABLE `tbl_pedido_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `carrinho_id` (`pedido_id`),
  ADD KEY `produto_id` (`produto_id`);

--
-- Índices para tabela `tbl_produto`
--
ALTER TABLE `tbl_produto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usuario_id` (`usuario_id`);

--
-- Índices para tabela `tbl_produto_categoria`
--
ALTER TABLE `tbl_produto_categoria`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categoria_id` (`categoria_id`),
  ADD KEY `produto_id` (`produto_id`);

--
-- Índices para tabela `tbl_produto_imagem`
--
ALTER TABLE `tbl_produto_imagem`
  ADD PRIMARY KEY (`id`),
  ADD KEY `produto_id` (`produto_id`);

--
-- Índices para tabela `tbl_promocao`
--
ALTER TABLE `tbl_promocao`
  ADD KEY `produto_id` (`produto_id`);

--
-- Índices para tabela `tbl_rede_social`
--
ALTER TABLE `tbl_rede_social`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `tbl_status_pedido`
--
ALTER TABLE `tbl_status_pedido`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `tbl_tipo_cobr`
--
ALTER TABLE `tbl_tipo_cobr`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `fat_pedido_cobranca`
--
ALTER TABLE `fat_pedido_cobranca`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `fat_status_pag`
--
ALTER TABLE `fat_status_pag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `sys_aplicacao`
--
ALTER TABLE `sys_aplicacao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `sys_cliente`
--
ALTER TABLE `sys_cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `sys_empresa`
--
ALTER TABLE `sys_empresa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `sys_empresa_config`
--
ALTER TABLE `sys_empresa_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `sys_menu`
--
ALTER TABLE `sys_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `tbl_carrinho`
--
ALTER TABLE `tbl_carrinho`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `tbl_carrinho_item`
--
ALTER TABLE `tbl_carrinho_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `tbl_categoria`
--
ALTER TABLE `tbl_categoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `tbl_cupom`
--
ALTER TABLE `tbl_cupom`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `tbl_dados_usuario`
--
ALTER TABLE `tbl_dados_usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `tbl_empresa_rede_social`
--
ALTER TABLE `tbl_empresa_rede_social`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `tbl_entrada`
--
ALTER TABLE `tbl_entrada`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `tbl_estoque`
--
ALTER TABLE `tbl_estoque`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `tbl_pedido`
--
ALTER TABLE `tbl_pedido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `tbl_pedido_item`
--
ALTER TABLE `tbl_pedido_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `tbl_produto`
--
ALTER TABLE `tbl_produto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `tbl_produto_categoria`
--
ALTER TABLE `tbl_produto_categoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `tbl_produto_imagem`
--
ALTER TABLE `tbl_produto_imagem`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `tbl_rede_social`
--
ALTER TABLE `tbl_rede_social`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `tbl_status_pedido`
--
ALTER TABLE `tbl_status_pedido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `tbl_tipo_cobr`
--
ALTER TABLE `tbl_tipo_cobr`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `fat_pedido_cobranca`
--
ALTER TABLE `fat_pedido_cobranca`
  ADD CONSTRAINT `fat_pedido_cobranca_ibfk_1` FOREIGN KEY (`pedido_id`) REFERENCES `tbl_pedido` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fat_pedido_cobranca_ibfk_2` FOREIGN KEY (`status`) REFERENCES `fat_status_pag` (`id`);

--
-- Limitadores para a tabela `fat_saida`
--
ALTER TABLE `fat_saida`
  ADD CONSTRAINT `fat_saida_ibfk_1` FOREIGN KEY (`cliente_id`) REFERENCES `sys_cliente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fat_saida_ibfk_2` FOREIGN KEY (`pedido_id`) REFERENCES `tbl_pedido` (`id`),
  ADD CONSTRAINT `fat_saida_ibfk_3` FOREIGN KEY (`empresa_id`) REFERENCES `sys_empresa` (`id`),
  ADD CONSTRAINT `fat_saida_ibfk_4` FOREIGN KEY (`produto_id`) REFERENCES `tbl_produto` (`id`);

--
-- Limitadores para a tabela `sys_aplicacao`
--
ALTER TABLE `sys_aplicacao`
  ADD CONSTRAINT `sys_aplicacao_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `sys_menu` (`id`);

--
-- Limitadores para a tabela `sys_cliente`
--
ALTER TABLE `sys_cliente`
  ADD CONSTRAINT `sys_cliente_ibfk_1` FOREIGN KEY (`dados_usuario_id`) REFERENCES `tbl_dados_usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sys_cliente_ibfk_2` FOREIGN KEY (`user`) REFERENCES `sys_usuario` (`user`);

--
-- Limitadores para a tabela `sys_empresa`
--
ALTER TABLE `sys_empresa`
  ADD CONSTRAINT `sys_empresa_ibfk_1` FOREIGN KEY (`dados_usuario_id`) REFERENCES `tbl_dados_usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sys_empresa_ibfk_2` FOREIGN KEY (`user`) REFERENCES `sys_usuario` (`user`);

--
-- Limitadores para a tabela `sys_empresa_config`
--
ALTER TABLE `sys_empresa_config`
  ADD CONSTRAINT `sys_empresa_config_ibfk_1` FOREIGN KEY (`empresa_id`) REFERENCES `sys_empresa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `sys_menu`
--
ALTER TABLE `sys_menu`
  ADD CONSTRAINT `sys_menu_ibfk_1` FOREIGN KEY (`empresa_id`) REFERENCES `sys_empresa` (`id`);

--
-- Limitadores para a tabela `sys_permissao`
--
ALTER TABLE `sys_permissao`
  ADD CONSTRAINT `sys_permissao_ibfk_1` FOREIGN KEY (`usuario`) REFERENCES `sys_usuario` (`user`),
  ADD CONSTRAINT `sys_permissao_ibfk_2` FOREIGN KEY (`aplicacao`) REFERENCES `sys_aplicacao` (`id`);

--
-- Limitadores para a tabela `sys_usuario_log`
--
ALTER TABLE `sys_usuario_log`
  ADD CONSTRAINT `sys_usuario_log_ibfk_1` FOREIGN KEY (`user`) REFERENCES `sys_usuario` (`user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `tbl_carrinho`
--
ALTER TABLE `tbl_carrinho`
  ADD CONSTRAINT `tbl_carrinho_ibfk_1` FOREIGN KEY (`empresa_id`) REFERENCES `sys_empresa` (`id`),
  ADD CONSTRAINT `tbl_carrinho_ibfk_2` FOREIGN KEY (`cliente_id`) REFERENCES `sys_cliente` (`id`);

--
-- Limitadores para a tabela `tbl_carrinho_item`
--
ALTER TABLE `tbl_carrinho_item`
  ADD CONSTRAINT `tbl_carrinho_item_ibfk_1` FOREIGN KEY (`carrinho_id`) REFERENCES `tbl_carrinho` (`id`),
  ADD CONSTRAINT `tbl_carrinho_item_ibfk_2` FOREIGN KEY (`produto_id`) REFERENCES `tbl_produto` (`id`);

--
-- Limitadores para a tabela `tbl_cupom`
--
ALTER TABLE `tbl_cupom`
  ADD CONSTRAINT `tbl_cupom_ibfk_1` FOREIGN KEY (`empresa_id`) REFERENCES `sys_empresa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `tbl_cupom_pedido`
--
ALTER TABLE `tbl_cupom_pedido`
  ADD CONSTRAINT `tbl_cupom_pedido_ibfk_1` FOREIGN KEY (`pedido_id`) REFERENCES `tbl_pedido` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_cupom_pedido_ibfk_2` FOREIGN KEY (`cupom_id`) REFERENCES `tbl_cupom` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `tbl_empresa_rede_social`
--
ALTER TABLE `tbl_empresa_rede_social`
  ADD CONSTRAINT `tbl_empresa_rede_social_ibfk_1` FOREIGN KEY (`empresa_id`) REFERENCES `sys_empresa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_empresa_rede_social_ibfk_2` FOREIGN KEY (`rede_social_id`) REFERENCES `tbl_rede_social` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `tbl_entrada`
--
ALTER TABLE `tbl_entrada`
  ADD CONSTRAINT `tbl_entrada_ibfk_1` FOREIGN KEY (`empresa_id`) REFERENCES `sys_empresa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_entrada_ibfk_2` FOREIGN KEY (`produto_id`) REFERENCES `tbl_produto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_entrada_ibfk_3` FOREIGN KEY (`usuario_id`) REFERENCES `sys_cliente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `tbl_estoque`
--
ALTER TABLE `tbl_estoque`
  ADD CONSTRAINT `tbl_estoque_ibfk_1` FOREIGN KEY (`empresa_id`) REFERENCES `sys_empresa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_estoque_ibfk_2` FOREIGN KEY (`produto_id`) REFERENCES `tbl_produto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `tbl_pedido`
--
ALTER TABLE `tbl_pedido`
  ADD CONSTRAINT `tbl_pedido_ibfk_1` FOREIGN KEY (`tipo_cobr_id`) REFERENCES `tbl_tipo_cobr` (`id`),
  ADD CONSTRAINT `tbl_pedido_ibfk_2` FOREIGN KEY (`empresa_id`) REFERENCES `sys_empresa` (`id`),
  ADD CONSTRAINT `tbl_pedido_ibfk_3` FOREIGN KEY (`status`) REFERENCES `tbl_status_pedido` (`id`),
  ADD CONSTRAINT `tbl_pedido_ibfk_4` FOREIGN KEY (`cliente_id`) REFERENCES `sys_cliente` (`id`);

--
-- Limitadores para a tabela `tbl_pedido_item`
--
ALTER TABLE `tbl_pedido_item`
  ADD CONSTRAINT `tbl_pedido_item_ibfk_1` FOREIGN KEY (`pedido_id`) REFERENCES `tbl_pedido` (`id`),
  ADD CONSTRAINT `tbl_pedido_item_ibfk_2` FOREIGN KEY (`produto_id`) REFERENCES `tbl_produto` (`id`);

--
-- Limitadores para a tabela `tbl_produto_categoria`
--
ALTER TABLE `tbl_produto_categoria`
  ADD CONSTRAINT `tbl_produto_categoria_ibfk_1` FOREIGN KEY (`categoria_id`) REFERENCES `tbl_categoria` (`id`),
  ADD CONSTRAINT `tbl_produto_categoria_ibfk_2` FOREIGN KEY (`produto_id`) REFERENCES `tbl_produto` (`id`);

--
-- Limitadores para a tabela `tbl_produto_imagem`
--
ALTER TABLE `tbl_produto_imagem`
  ADD CONSTRAINT `tbl_produto_imagem_ibfk_1` FOREIGN KEY (`produto_id`) REFERENCES `tbl_produto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `tbl_promocao`
--
ALTER TABLE `tbl_promocao`
  ADD CONSTRAINT `tbl_promocao_ibfk_1` FOREIGN KEY (`produto_id`) REFERENCES `tbl_produto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
