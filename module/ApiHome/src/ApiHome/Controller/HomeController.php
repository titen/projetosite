<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace ApiHome\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use ApiHome\Classes\Funcoes;

class HomeController extends AbstractActionController {

    public function iniciarSessaoAction() {
        try {
            $funcao = new Funcoes($this);

            if (!$funcao->validaAcesso($this->params()->fromPost('pass', ''))) {
                return new JsonModel(array('status' => 401, 'msg' => utf8_encode('Acesso n�o permitido!')));
            }

            $idPedidoTemp = 'LO' . date("YmdHisAwzNjT");
            return new JsonModel(array('idPedidoTemp' => $idPedidoTemp));
        } catch (Exception $e) {
            return new JsonModel(array('response' => false, 'msg' => utf8_encode('Ocorreu um erro ao realizar o processamento')));
        }
    }
    
    public function loginAction() {
        try {
            
          
           $funcao = new Funcoes($this);
            
           $rs = $funcao->executarSQL('select now()',[],'all','orm_hinode_desenv');
           
            return new JsonModel($rs);
        } catch (Exception $e) {
            return new JsonModel(array('response' => false, 'msg' => utf8_encode('Ocorreu um erro ao realizar o processamento')));
        }
    }

    public function HomeAction() {
        try {
            $pais   = $this->params()->fromPost('pais', 1);
            $ecommerce   = $this->params()->fromPost('ecommerce', 1);
            $canal= ($ecommerce == 0)? '7':'9';
            
            $funcao = new Funcoes($this,$pais);
           
            if (!$funcao->validaAcesso($this->params()->fromPost('pass', ''))) {
                return new JsonModel(array('status' => 401, 'msg' => utf8_encode('Acesso n�o permitido!')));
            }
            
            $site   = $this->params()->fromPost('site', 0);
            $site = $site+1;
            $mais_buscados = [];
            $mais_buscadosHND = [];
            
            $produtos = $funcao->utf8_converter($this->Listaprodutos());
            
            foreach ($produtos as $row) {
                if ($row['tipo'] == '4000') {
                    array_push($mais_buscados, $row);
                } else if ($row['tipo'] == '4010') {
                    array_push($mais_buscadosHND, $row);
                }
            }
            $menu = $this->gerarMenuNovo($pais,$site,$canal);
           
            if ($site==2){
                $mais_buscados = $mais_buscadosHND;
            }
            $mais_vendidos = $funcao->utf8_converter($this->listaMaisVendidos($pais,$site));
            $selecaoEspecial = $funcao->utf8_converter($this->selecaoEspecial($pais,$site));
            $novidades = $funcao->utf8_converter($this->lancamento($pais,$site));
           
            return new JsonModel(array(
                'menu' => $menu,
                'lancamento' => $novidades,
                'maisvendidos' => $mais_vendidos,
                'selecaoespecial' => $selecaoEspecial,
                'buscados' => $mais_buscados,
            ));
        } catch (Exception $e) {
            return new JsonModel(array('response' => false, 'msg' => utf8_encode('Ocorreu um erro ao realizar o processamento')));
        }
    }

    private function gerarMenuNovo($pais,$site,$canal) {
        $funcao = new Funcoes($this,$pais);
          
//        $sql = "exec dbo.listarMenu_sp :pais,:site,:canal";  //producao
        $sql = "exec dbo.listarMenu2_sp :pais,:site,:canal"; //homologacao
        
        $params = array(
            'pais'=> $pais,
            'site'=> $site,
            'canal'=> $canal
        );
        
        $result = $funcao->executarSQL($sql,$params);
        $dados = [];
        $div="";
        $cat="";
        
        foreach ($result as $row) {
            
            if ($div<>$row['divisao']){ 
                array_push($dados, array(
                    'id' => $row['divisao_id'],
                    'descricao' => utf8_encode($row['div_descricao']),
                    'id_nome' => utf8_encode($row['divisao']),
                    'codigo_pagina' => md5(utf8_encode($row['divisao'])),
                    'filhos' => []
                ));
                $div=$row['divisao'];
                $cat="";
            }
            
            if ($cat<>$row['categoria']){
                $cont1= count($dados);
                array_push($dados[$cont1-1]['filhos'], array(
                    'id' => $row['uso_id'],
                    'descricao' => utf8_encode($row['uso_descricao']),
                    'id_nome' => utf8_encode($row['categoria']),
                    'codigo_pagina' => md5(utf8_encode($row['divisao'])."|". utf8_encode($row['categoria'])),
                    'filhos' => []
                ));
                
                $cat = $row['categoria'];
            }
            $cont2= count($dados[$cont1-1]['filhos']);
            array_push($dados[$cont1-1]['filhos'][$cont2-1]['filhos'], array(
                'id' => $row['linha_id'],
                'descricao' => utf8_encode($row['linha_descricao']),
                'id_nome' => utf8_encode($row['linha']),
                'codigo_pagina' => md5(utf8_encode($row['divisao'])."|". utf8_encode($row['categoria'])."|".utf8_encode($row['linha'])),
            ));       
        }
        
        return $dados;
    }

    //retorna cod e descricao dos paises;
    public function listarPaisesAction() {
        try {
            $funcao = new Funcoes($this);


            $codPais = $this->params()->fromPost('cod', '');

            $pais = $funcao->utf8_converter($this->pais($codPais));

            return new JsonModel(array(
                'pais' => $pais,
            ));
        } catch (Exception $e) {
            return new JsonModel(array('response' => false, 'msg' => utf8_encode('Ocorreu um erro ao realizar o processamento')));
        }
    }

    private function pais($pais) {
        $funcao = new Funcoes($this);
        $sql = "exec dbo.loja_listarPaises_sp :cod";
        $params = array(
            'cod' => $pais
        );
        $result = $funcao->executarSQL($sql, $params);
        return $result;
    }

    private function ListaMaisVendidos($pais,$site) {
        $funcao = new Funcoes($this);
        $params = array(
            'pais' => $pais,
            'site' => $site
        );
        
        $sql = "exec dbo.loja_produtosMaisVendidos_sp :pais,:site";
        
        $result = $funcao->executarSQL($sql,$params);

        $dados = [];

        foreach ($result as $rows) {

            array_push($dados, array(
                'idProduto' => $rows['idProduto'],
                'codigo' => $rows['codigo'],
                'Nome' => $rows['Nome'],
                'valor' => $rows['valor'],
                'idDivisao' => $rows['idDivisao'],
                'Divisao' => $rows['Divisao'],
                'foto' => $rows['foto'],
                'estoque' => $rows['estoque'],
                
            ));
        }
        return $dados;
    }

    private function selecaoEspecial($pais,$site) {
        $funcao = new Funcoes($this);
       
        if ($site==1){
            $sql = "exec dbo.loja_produtoSelecaoEspecial_sp";
        }else{
            $sql = "exec dbo.loja_produtoSelecaoEspecialHND_sp";
        }
        $result = $funcao->executarSQL($sql);

        if (!defined('APPLICATION_PATH')) {
            define('APPLICATION_PATH', realpath(__DIR__ . '/../../../../../../'));
        }

        $dados = [];
        foreach ($result as $rows) {

            if ($rows['foto'] <> 'http://i-corp.hinode.com.br/fotoproduto/0_p.png') {
                $foto = $rows['foto'];
            } else {
                if (file_exists(APPLICATION_PATH . '/online.hinode.com.br/produtos/' . $rows['codigo'] . '_g.jpg')) {
                    $foto = 'https://online.hinode.com.br/produtos/' . $rows['codigo'] . '_g.jpg';
                } else {
                    $foto = $rows['foto'];
                }
            }

            array_push($dados, array(
                'idProduto' => $rows['idProduto'],
                'codigo' => $rows['codigo'],
                'Nome' => $rows['Nome'],
                'valor' => $rows['valor'],
                'foto' => $foto,
            ));
        }
        return $dados;
    }

    
    private function lancamento($pais,$site) {
        $funcao = new Funcoes($this);
        $params = array(
            'pais'=> $pais,
            'site'=> $site-1
        );
        $sql = "exec dbo.loja_buscarlancamento_sp :site,:pais";

        $result = $funcao->executarSQL($sql,$params);

        $dados = [];
        foreach ($result as $rows) {
            array_push($dados, array(
                'idProduto' => $rows['idProduto'],
                'codigo' => $rows['codigo'],
                'Nome' => $rows['Nome'],
                'valor' => $rows['valor'],
                'foto' => $rows['foto'],
            ));
        }
        return $dados;
    }

    private function Listaprodutos() {
        $funcao = new Funcoes($this);
        $sql = "exec dbo.loja_listaProdutosHome_sp";
        $result = $funcao->executarSQL($sql);

        $dados = [];
        foreach ($result as $rows) {
            array_push($dados, array(
                'idProduto' => $rows['idProduto'],
                'codigo' => $rows['codigo'],
                'Nome' => $rows['Nome'],
                'valor' => $rows['valor'],
                'foto' => $rows['foto'],
                'tipo' => $rows['tipo'],
            ));
        }
        return $dados;
    }

    public function menuCategoriaAction() {
        try {
            $pais   = $this->params()->fromPost('pais', 1);
            $ecommerce   = $this->params()->fromPost('ecommerce', 1);
            $canal= ($ecommerce == 0)? '7':'9';
            
            
            $funcao = new Funcoes($this,$pais);
           
            if (!$funcao->validaAcesso($this->params()->fromPost('pass', ''))) {
                return new JsonModel(array('status' => 401, 'msg' => utf8_encode('Acesso n�o permitido!')));
            }
            
            $divisaoNome = $this->params()->fromPost('divisao', '');
            $categoriaNome = $this->params()->fromPost('categoria', '');
            $subcategoriaNome = $this->params()->fromPost('subcategoria', '');
           
            $pagina = $this->params()->fromPost('pagina', '1');
            $qtd = $this->params()->fromPost('qtd', '20');

            $titulo = [];

            $sql = "exec dbo.loja_breadcrumbCategoria2_sp :divisao,:categoria,:linha,:pais ";
            $params = array(
                'divisao' => $divisaoNome,
                'categoria' => $categoriaNome,
                'linha' => $subcategoriaNome,
                'pais' => $pais,
            );
            
            $codigo_pagina=$divisaoNome;
            if ($categoriaNome<>""){
                $codigo_pagina.="|".$categoriaNome;
                if ($subcategoriaNome<>""){
                    $codigo_pagina.="|".$subcategoriaNome;
                }
            }
            
            $codigo_pagina=md5($codigo_pagina);

            $pag = $funcao->executarSQL($sql, $params, '');
            
            $categoria = -1;
            $subcategoria = -1;
            $divisao = -1;
 
            $pag = $funcao->utf8_converter($pag, '');
            
            if ($pag) {
                if ($pag['iddiv']) {
                    $titulo['id'] = $pag['iddiv'];
                    $titulo['id_nome'] = $divisaoNome;
                    $titulo['descricao'] = utf8_encode($pag['div']);
                    $titulo['filho'] = [];
                    $divisao = $pag['iddiv'];
                }
                if ($pag['idcat']) {
                    $titulo['filho']['id'] = $pag['idcat'];
                    $titulo['filho']['id_nome'] = $categoriaNome;
                    $titulo['filho']['descricao'] = utf8_encode(ucwords(strtolower($pag['cat'])));
                    $titulo['filho']['filho'] = [];
                    $categoria = $pag['idcat'];
                }
                if ($pag['idlin']) {
                    $titulo['filho']['filho']['id'] =  $pag['idlin'];
                    $titulo['filho']['filho']['id_nome'] = $subcategoriaNome;
                    $titulo['filho']['filho']['descricao'] = utf8_encode(ucwords(strtolower($pag['linha'])));
                    $subcategoria = $pag['idlin'];
                }
            } else {
                return new JsonModel(array('response' => false, 'msg' => 'Resultado da busca vazio!'));
            }
            
            if ($categoria == '') {
                $categoria = -1;
            }

            if (!defined('APPLICATION_PATH')) {
                define('APPLICATION_PATH', realpath(__DIR__ . '/../../../../../../'));
            }

            $sql = "exec dbo.loja_buscarfiltrosProdutos_sp :divisao, :categoria, :linha, :pais,:canal";

            $params = array(
                'categoria' => $categoria,
                'linha' => $subcategoria,
                'divisao' => $divisao,
                'pagina' => $pagina,
                'qtd' => $qtd,
                'pais' => $pais,
                'canal' => $canal
            );
            
            $result = $funcao->executarSQL($sql, $params);

            if (!$result) {
                return new JsonModel(array('response' => false, 'msg' => 'Resultado da busca vazio!'));
            }
            $result = $funcao->utf8_converter($result);

            $categorias = [];
            $volumetria = [];
            $marcas = [];
            $tipos = [];
            $valor = [];

            foreach ($result as $rows) {

                if ($rows['grupo'] == 'marca') {
                    array_push($marcas, array(
                        "idLinha" => $rows['id'],
                        "linha" => $rows['descricao']
                    ));
                }
                if ($rows['grupo'] == 'categoria') {
                    array_push($categorias, array(
                        "idCategoria" => $rows['id'],
                        "categoria" => $rows['descricao']
                    ));
                }

                if ($rows['grupo'] == 'volumetria') {
                    array_push($volumetria, array(
                        "idVolumetria" => $rows['id'],
                        "volumetria" => $rows['descricao']
                    ));
                }

                if ($rows['grupo'] == 'valor') {
                    array_push($valor, array(
                        "idValor" => $rows['id'],
                        "valor" => $rows['descricao']
                    ));
                }

                if ($rows['grupo'] == 'tipo') {
                    array_push($tipos, array(
                        "idTipo" => $rows['id'],
                        "tipo" => $rows['descricao']
                    ));
                }
            }

            $sql = "exec dbo.loja_filtrarProdutosBarra_sp  @divisao = :divisao,@categoria = :categoria,@linha=:linha,@pagina=:pagina,@qtd=:qtd,@pais = :pais,@canal=:canal";
           
            $result = $funcao->executarSQL($sql, $params);
            
            $totalRetorno = $result[0]['total'];
            $result = $funcao->utf8_converter($result);
            $pai = $result[0]['pai'];
            $dados = [];
            $grupos = [];
            foreach ($result as $rows) {

                if ($rows['pai'] <> $pai) {
                    array_push($dados, $grupos);
                    $grupos = [];
                    $pai = $rows['pai'];
                }
                
                array_push($grupos, array(
                    'codigo' => $rows['codigo'],
                    'idProduto' => $rows['idProduto'],
                    'foto' => $rows['foto'],
                    'Nome' => $rows['nome'],
                    'cor' => $rows['cor'],
                    'valor' => number_format($rows['valor'], 2, ",", ""),
                    'estoque' => $rows['estoque'],
                ));
            }
            array_push($dados, $grupos);
            
            return new JsonModel(array(
                'response' => true, 
                'codigo_pagina' => $codigo_pagina,
                'titulo' => $titulo, 
                'title' => $pag['title'], 'meta' => $pag['meta'], 
                'produtos' => $dados,
                'totalProduto' => $totalRetorno,
                'categorias' => $categorias, 'marcas' => $marcas, 'tipos' => $tipos, 
                'volumetria' => $volumetria, 'valor' => $valor
            ));

        } catch (Exception $e) {
            return new JsonModel(array('response' => false, 'msg' => utf8_encode('Ocorreu um erro ao realizar o processamento')));
        }
    }

    public function getEntityManager($connector = "orm_default") {
        $this->em = $this->getServiceLocator()->get('doctrine.entitymanager.' . $connector);
        return $this->em;
    }
}
