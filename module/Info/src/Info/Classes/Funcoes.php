<?php

namespace ApiHome\Classes;

use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Exception;
use Zend\View\Model\JsonModel;
Use Zend\Crypt\BlockCipher;

class Funcoes {

    protected $em;
    protected $objeto;

//     public function getEntityManager($connector = "orm_default") {
//        $this->em = $this->getServiceLocator()->get('doctrine.entitymanager.' . $connector);
//        return $this->em;
//    }
    protected function getEntityManager($connector = "orm_default") {
        $this->em = $this->objeto->getServiceLocator()->get('doctrine.entitymanager.' . $connector);
        return $this->em;
    }
    
    public function __construct($objeto = null,$pais = 1) {
        $this->objeto = $objeto;
        
        if ($objeto != null) {
            $this->tradutor = $this->objeto->getServiceLocator()->get('translator');
        }
        
        $params = array(
            'pais' => $pais,
        );
        $sql= 'select linguagem from hinode.dbo.sys_idiomavo where codigo=:pais'; 
        $stmt = $this->executarSQL($sql, $params,'');
//        echo "<pre>";
//        var_dump($stmt);
//        echo "</pre>";
        $this->getTranslate()->setLocale($stmt['linguagem']);
    }

    public function getTranslate() {
        return $this->tradutor;
    }
    
     public function alertBasic($message = 'Access Denied', $close = false, $redirect = false, $type = 'warning', $titulo = 'Aten��o!') {
        $service_translate = $this->getTranslate();
        $titulo =  $service_translate && $titulo ? $service_translate->translate($titulo) : $titulo;
        
        header('Content-type: text/html; charset=CP1252');

        echo "<script src=\"/js/sweetalert.min.js\"></script>";
        echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"/css/sweetalert.css\">";
        echo "<body><script>swal({title:'$titulo',text:'$message', type:'$type', html: true}, function(){" . ($close ? "window.close(); " : ($redirect ? "location.href='$redirect';": 'history.back();')) ."});</script></body>";
        exit;
    }
    
    public function enviarEmail($email, $corpo, $assunto) {
        try {
            $htmlPart = new MimePart($corpo);
            $htmlPart->type = "text/html";

            $body = new MimeMessage();
            $body->setParts(array($htmlPart));

            $message = new Message();
            $message->addTo($email)
                    ->addFrom('naoresponda@kplay.com.br')
                    ->setEncoding("utf-8")
                    ->setSubject($assunto)
                    ->setBody($body);

            $transport = new SmtpTransport();
            $options = new SmtpOptions(array(
                'host' => 'smtp.kplay.com.br',
                'port' => '25',
                'connection_class' => 'login',
                'connection_config' => array(
                    'username' => 'naoresponda@kplay.com.br',
                    'password' => 'kplay2013@99',
                ),
            ));
            $transport->setOptions($options);
            $transport->send($message);
            return 'Email enviado para ' . $email . '!';
        } catch (Exception $e) {
            return 'Algo ocorreu errado ao enviar sua recupera��o! Contate o administrador!';
        }
    }
    
    function utf8_converter($array = array(), $opcao = 'all', $tipo = 'encode') {
        $array2 = array();
        if ($array){
            foreach ($array as $key => $value) {
                if (is_array($value)){
                    $array2[strtolower($key)] = $this->utf8_converter($value,$tipo);
                } else {
                    if ($tipo=='encode'){
                        $array2[strtolower($key)] = utf8_encode($value); 
                    } else if ($tipo=='decode'){
                        $array2[strtolower($key)] = utf8_decode($value);
                    } else {
                        $array2[strtolower($key)] = ($value);
                    }
                }
            }
        }
        return $array2;
    }    
    
    function validaAcesso($pass) {

        if ($pass <> '') {
            $key = 'webTeste@hinode';
            $key2 = "bf0cba7cdacaf63b";

            $encrypt_method = "AES-256-CBC";

//            action 'encrypt' 
//            $encrypt = openssl_encrypt($dado, $encrypt_method, $key, 0, $key2);
//            action == 'decrypt' 
            $decrypt = openssl_decrypt($pass, $encrypt_method, $key, 0, $key2);

            if (is_numeric($decrypt)) {
                if ((time() - 30) < $decrypt) {
                    return true;
                }
            }
        }
        return true;
    }
    
    function geraPassAcesso() {

        $key = 'webTeste@hinode';
        $key2 = "bf0cba7cdacaf63b";

        $encrypt_method = "AES-256-CBC";
        $dado = time();
        $encrypt = openssl_encrypt($dado, $encrypt_method, $key, 0, $key2);


        return $encrypt;
    }

    public function getSQLBinded($sql, $params) {
        $indexed = $params == array_values($params);
        foreach ($params as $k => $v) {
            if (is_string($v))
                $v = "'$v'";
            if ($indexed)
                $sql = preg_replace('/\?/', $v, $sql, 1);
            else
                $sql = preg_replace("/:$k\b/i", $v, $sql);
        }
        return $sql;
    }

    public function executarSQL($query = '', $params = array(), $tipo = 'all', $connector = "orm_hinode_loja") {
        if (!isset($this->em))
            $conn = $this->getEntityManager($connector)->getConnection();
        else
            $conn = $this->em->getConnection();
        $parametros = $this->utf8_converter($params, '', 'decode');
        $stmt = $conn->executeQuery($query, $parametros);

        if ($tipo == 'all') {
            $dados = $stmt->fetchAll();
        } else if ($tipo == 'stmt') {
            return $stmt;
        } else {
            $dados = $stmt->fetch();
        }
        return $dados;
    }

    public function existeArquivo($url) {
        $validar = get_headers($url);
        $validar = explode(" ", $validar[0]);
        $validar = $validar[1];
        if ($validar == "302" || $validar == "200")
            return true;
        else
            return false;
    }

    public function logErroProcedure($emitente, $usuario, $msg) {
        $params = array(
            'emitente' => $emitente,
            'usuario' => $usuario,
            'msg' => $msg,
            'obs' => $this->objeto->params('controller') . "\\" . $this->objeto->params('action')
        );

        $connection = $this->getEntityManager("orm_hinode_desenv")->getConnection();
        $sql = "INSERT INTO dbo.sys_ErroProcedure(emitente,mensagem,data,usuario,obs) VALUES (:emitente, :msg, GETDATE(), :usuario, :obs)";
        $stmt = $connection->executeQuery($sql, $params);
        $stmt->fetch();
    }

}
