<?php

namespace ApiHome\Classes;

use Zend\Http\Client;

class PayU{    
    private $merchant_id;    
    private $api_key;    
    private $api_login;                
    private $account_id;
    private $url_payments;
    private $url_queries;
        
    
    public function getUrlPayment($payment = true){        
        return $payment ? $this->url_payments : $this->url_queries;
    }
    
    function __construct($production = false) {                                
        if($production){                 
            $this->url_payments = "https://api.payulatam.com/payments-api/4.0/service.cgi";            
            $this->url_queries = "https://api.payulatam.com/reports-api/4.0/service.cgi";              
        } else{
            $this->url_payments = "https://sandbox.api.payulatam.com/payments-api/4.0/service.cgi";
            $this->url_queries = "https://sandbox.api.payulatam.com/reports-api/4.0/service.cgi";
        }        
        $this->env = $production;
    }
    
    protected function isValid($array){        
        return ($array && is_array($array)) ? $array : false;
    }
    
    
//    
//    public function getResult($smarkio_url, $leads = 'leads'){                
//        $result = file_get_contents($this->getLink($smarkio_url), NULL, stream_context_create(
//                array(
//                    'ssl' => array(
//                        'verify_peer' => false,
//                        'verify_peer_name' => false,
//                        'allow_self_signed' => true
//                    ),
//                    'http' => array(
//                        'timeout' => 15
//                    )
//                )
//            )
//        );
//        return $this->isValid(json_decode($result, true)[$leads]);    
//    }
    
    public function sendPost($json, $payment = true, $return = 'array'){
        $options = array(
            'adapter' => 'Zend\Http\Client\Adapter\Curl',
            'curloptions' => array(
                //SSL
                CURLOPT_FOLLOWLOCATION => TRUE,
                CURLOPT_SSL_VERIFYPEER => FALSE,
                
                //Supported encodings are "identity", "deflate", and "gzip"
                CURLOPT_ENCODING => ''
            ),
            'timeout' => 61,
            'keepalive' => true            
        );
        
        $client = new Client($this->getUrlPayment($payment), $options);
        $client->setMethod('POST');
        $client->setRawBody($json);
        $client->setEncType('application/json');
//        $client->setHeaders(array(
//            'content-type' => 'application/json; charset=utf-8'
//        ));
                        
        $response = $client->send()->getContent();
                                
        $json = json_encode(simplexml_load_string($response, "SimpleXMLElement", LIBXML_NOCDATA));                   
        
        return $return == 'array' ? $this->isValid(json_decode($json, TRUE)) : $json;
    }        
}


