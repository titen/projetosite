<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Info\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Http\PhpEnvironment\RemoteAddress;

class InfoController extends AbstractActionController{
    
    public function sobreAction(){
        
        echo "<script>

            function loadBalance(server){
                function getCookie(name) {
                  var value = \"; \" + document.cookie;
                  var parts = value.split(\"; \" + name + \"=\");
                  if (parts.length >= 2) return parts.pop().split(\";\").shift();
                }

                if(server == 1){
                    server = \"HNVO02CK01XYZ\";
                }else if(server == 2){
                    server = \"HNVO02CK02XPT\";
                }

                var x = getCookie('PHPSESSIONID');
                x = x.substring(13, 25);    
                x = \"PHPSESSIONID=\"+server+x+\"; expires=; path=/\";

                document.cookie = x;
                
                location.reload();
            }

            </script>";
                      
//        $translator = $this->getServiceLocator()->get('translator');
        echo "Seu IP:" .(new RemoteAddress())->getIpAddress();

        phpinfo();
        exit;
    }   
    
    
    public function getEntityManager($connector = "orm_default") {
        $this->em = $this->getServiceLocator()->get('doctrine.entitymanager.' . $connector);
        return $this->em;
    }
}
