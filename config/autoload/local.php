<?php
//$db = array(
//    'database' => 'DEMONSTRACAO',
//    'username' => 'demonstracao',
//    'password' => 'apoxkca@#pqx87',
//    'hostname' => 'sqlserver01.kplay.com.br',
//    'port' => '51100'
//);
return array(
    'doctrine' => array(
        'connection' => array(

            'orm_hinode_desenv' => array(
               'driverClass' => 'Doctrine\DBAL\Driver\PDOMySql\Driver',
                'params' => array(
                    'host'     => '127.0.0.1',
                    'port'     => '3306',
                    'user'     => 'root',
                    'password' => '',
                    'dbname'   => 'bd_projetosite',
                    'driverOptions' => array(
                        1002 => 'SET NAMES utf8'
                    ),
                ),
            ),
            
            'orm_hinode_loja' => array(
                'driverClass' => 'Doctrine\DBAL\Driver\Sqlsrv\Driver',
                'params' => array(
                    'host' => '177.154.134.115',
                    #'port' => $db['port'],
                    'user' => 'hinode_icorp',
                    'password' => 'n#,21zpoq4ax',
                    'dbname' => 'hinode_loja',
                )
            )
        ),
        
        'entitymanager' => array(
            'orm_hinode_desenv' => array(
                'connection'    => 'orm_hinode_desenv',
                'configuration' => 'orm_hinode_desenv',
            ),
            
            'orm_hinode_loja' => array(
                'connection'    => 'orm_hinode_loja',
                'configuration' => 'orm_hinode_loja',
            ),
            
            
        ),
        
        'configuration' => array(
            'orm_hinode_desenv' => array(
                'metadata_cache' => 'array',
                'query_cache' => 'array',
                'result_cache' => 'array',
                'hydration_cache' => 'array',
                'generate_proxies' => true,
            ),
            
            
            'orm_hinode_loja' => array(
                'metadata_cache' => 'array',
                'query_cache' => 'array',
                'result_cache' => 'array',
                'hydration_cache' => 'array',
                'generate_proxies' => true,
            ),
        ),
    ),
);

